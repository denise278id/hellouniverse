﻿using System.Diagnostics;
using Xamarin.Forms;

namespace HelloUniverse
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new HelloUniversePage();
        }

        protected override void OnStart()
        {
            //alt enter
            Debug.WriteLine($"*****{this.GetType().Name}.{nameof(OnStart)}");
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Debug.WriteLine($"*****{this.GetType().Name}.{nameof(OnSleep)}");
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            Debug.WriteLine($"*****{this.GetType().Name}.{nameof(OnResume)}");
        }
    }
}
